<?php 

    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    
    class Connection{
        // session_start();
        //echo "su identificador de sesión es ".session_id();
        
        private $user;
        private $password;
        private $db;
        private $host;
        private $port;
        private $charset;
        
        public function __construct(){
            $this->host = "localhost";
            $this->db = "DB_gp";
            $this->user = "root";
            $this->password = "root";
            $this->port = "3306";
            $this->charset = "utf8mb4";
        }
        function connect() {
            try {
                $connection = "mysql:host=" . $this->host . ";dbname=". $this->db . ";charset=" . $this->charset;
                

                $options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                            PDO::ATTR_EMULATE_PREPARES => false];
                
                $pdo = new PDO($connection, $this->user, $this->password, $options);
                return $pdo;
                
            }catch(PDOException $e){
                print_r('Connection error: ' . $e->getMessage());
            }
        }
    }
        
?>