import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map} from 'rxjs/operators';
import { IUser } from '../../app/models/users/user';

interface isLoggedInServer{
  status: boolean
}
@Injectable({
  providedIn: 'root'
})




export class LoginServiceService {
 

  user: IUser;

  private loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');


 // private urlService: any = "http://localhost:8888"; // MACOS
  private urlService: any = "http://localhost:8090"; // windows
  private urlDirection: any = "/api/apis/apiUser.php";
  private resObj: any;
  public _headers = new HttpHeaders({
    'Content-Type':  'application/x-www-form-urlencoded',
  });
 
  
  constructor(private http: HttpClient) {
    console.log("login service started");
  }

  promiseEjemp() {    // ejemplo para regresar una promesa

    var promise = new Promise((resolve, reject) => {
      var error = false;
      if (!error) {
        resolve();
      } else {
        reject('something went wrong');
      }
    });
    return promise;
  }

  userAuth(user:string,pass:string): Observable<any> {   //user authentification
    var type = 'auth';
    var url = this.urlService + this.urlDirection;
    
    let body = new HttpParams()
    .set('tipo', type)
    .set('user', user)
    .set('pass', pass);
    
    var res = this.http.post<IUser>(url, body, {headers:  this._headers , responseType: 'text' as 'json'}).pipe(
      catchError(this.handleError)//map(data=>data) // 
       );
  
    return res;
  }

  userLogOut(): any {
    let body = new HttpParams().set('tipo','logauth');
    var url = this.urlService + this.urlDirection;
    return this.http.post(url,body, {headers:  this._headers , responseType: 'text' as 'json'}).pipe(
      catchError(this.handleError)
    );
  }


  setLoggedIn(value: boolean,type : string,user:string,username:string) {
    this.loggedInStatus = value;
    localStorage.setItem('loggedIn', 'true');
    localStorage.setItem('name', user);
    localStorage.setItem('username', username);
    localStorage.setItem('usertype',type);
  }

  get isLoggedInLocalStorage() {
    if (localStorage.getItem('loggedIn')){
      return JSON.parse(localStorage.getItem('loggedIn') || this.loggedInStatus.toString());
    }
  }

  isLoggedInServer(): Observable<any> {
    let body = new HttpParams().set('tipo','getSession');
    var url = this.urlService + this.urlDirection;
    return this.http.post<isLoggedInServer>(url,body, {headers:  this._headers , responseType: 'text' as 'json'}).pipe(
      catchError(this.handleError)
    );
  }



  /////////////////////Error handler 

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };



}
