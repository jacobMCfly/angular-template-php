import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

//Material design
import { MaterialAppModule } from './ngmaterial.module';

//Global commponents entry
import { MyDialogComponent } from './my-dialog/my-dialog.component';
import { SnackBarComponent } from './snack-bar/snack-bar.component';

//Pages componnents
import { AppComponent } from './app.component'
import { LoginComponent } from '../pages/login/login.component';
import { HomeComponent } from '../pages/home/home.component';

// Services providers
import { HttpClientModule,HttpHeaders } from '@angular/common/http';
import {LoginServiceService} from '../services/login/login-service.service'; 
//routers and routs
import { AppRoutingModule } from './app-routing.module';



// auth Guard
import {AuthGuard} from './auth.guard';


const appRoutes: Routes = [
  { path:'', component: LoginComponent},
  { path:'login', component: LoginComponent},  

  { path:'home', component: HomeComponent,canActivate: [AuthGuard]}   //solo con autentificación
];

@NgModule({
  declarations: [
    AppComponent,
    MyDialogComponent,
    SnackBarComponent,
    HomeComponent,
    LoginComponent
 
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule, 
    BrowserAnimationsModule,
    MaterialAppModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    LoginServiceService,
    AuthGuard, 
  ],
  bootstrap: [AppComponent],
  entryComponents :[MyDialogComponent,SnackBarComponent]
})
export class AppModule { }
