import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import {LoginServiceService} from '../services/login/login-service.service';
import {map} from 'rxjs/operators';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  
  constructor(private loginService:LoginServiceService, private router: Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean | any {
    if (this.loginService.isLoggedInLocalStorage){ // inicio sesion guarda true en variable de localstorage
      
      this.loginService.isLoggedInServer().subscribe(data=>{
        var obj = JSON.parse(data);
        console.log(obj);
        if (obj.status){
          return true;
        }else {
          this.router.navigate(['login']);
          return false
        } 
      });
     
    }else {
       this.loginService.isLoggedInServer().subscribe(data =>{
        var obj = JSON.parse(data);
        console.log(obj);
        if (obj.status){
          return true;
        }else {
          this.router.navigate(['login']);
          return false
        } 
       })
    } 
      
  }
}
