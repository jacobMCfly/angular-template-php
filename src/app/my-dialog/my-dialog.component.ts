import { Component, OnInit,Input,Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
@Component({
  selector: 'app-my-dialog',
  templateUrl: './my-dialog.component.html',
  styleUrls: ['./my-dialog.component.scss']
})
export class MyDialogComponent implements OnInit {
 
  options = false;
  title = false;
  content :any = "";
  titleValue:any = {
    text: ""
  };
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
  
      console.log(data);
      this.title = data.object.title ;
      this.titleValue.text = data.object.titleValue ;
      this.content = data.object.contentValue;
      this.options = data.object.options;

      console.log("este es el valor recibido del texto;");
      console.log(this.title);
      console.log(this.titleValue.text);
      console.log(this.content);
      console.log(this.options);
    }

  ngOnInit() {
  }

}
