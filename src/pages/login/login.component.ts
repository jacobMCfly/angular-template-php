import { Component, OnInit } from '@angular/core';
import {MatDialog, MatSnackBar} from '@angular/material';
import {MyDialogComponent} from 'src/app/my-dialog/my-dialog.component'
import {LoginServiceService} from 'src/services/login/login-service.service';
import { Router,ActivatedRoute } from '@angular/router'; 
import { from } from 'rxjs';
import { IUser } from 'src/app/models/users/user';
 
import { isUndefined } from 'util';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
 
  title = 'Joaquin';  
  enableButtonLogin: any =false ;
  user:any;
  pass:any;
  
 
  

  constructor(  private dataRoute: ActivatedRoute, private router : Router,public dialog: MatDialog, public snackbar: MatSnackBar, private _servicioLogin:LoginServiceService) {
   //   this.example = JSON.parse(this.dataRoute.snapshot.params['data']);
     // console.log(this.example);
     
   }

  ngOnInit() {
  }

  //////////////////////////////////////////////////////////////////////////////////// COMPONENTS
  openSnackBar(message: string, action: string) {

    this.snackbar.open(message, action, {
      duration: 2000,
    });
  }

  openDialog(title:boolean, titleValue:string, contentValue:string, options:boolean):void{   //// dialogo
    var dalogParams= {
      title :title,
      titleValue: titleValue,
      contentValue: contentValue,
      options:options
    }
    let dialogRef = this.dialog.open(MyDialogComponent,{data:{dalogParams}});
 
    
    dialogRef.afterClosed().subscribe(result=>{
      console.log("dialogo cerrado");
   
    });
  }
//////////////////////////////////////////////////////////////////////////////////// END COMPONENTS
  login(){
    if ( this.user && this.pass){
  //    this.openDialog(true,"Atención","este es un dialogo",false);
        
        this._servicioLogin.userAuth(this.user,this.pass).subscribe(
          (data) =>{
            var jsonArrayObj = JSON.parse(data);
            console.log(jsonArrayObj); 
            if(jsonArrayObj.user){
              this.openSnackBar("Usuario o Contraseña inválida","Ok");
              return false;
            } 
            if (jsonArrayObj[0].Usuario){
              this.router.navigate(['home']);
              this._servicioLogin.setLoggedIn(true,jsonArrayObj[0].Tipo_usuario,jsonArrayObj[0].Nombre,jsonArrayObj[0].Usuario);
            } 
          }
      );
      
    }
    else{
      this.openSnackBar("Error falta un campo","Ok");
    }
    
  }
  exit(){
    this._servicioLogin.userLogOut().subscribe(
      (data) =>{
        console.log(data);
        console.log(this._servicioLogin.isLoggedInLocalStorage);
        if ( this._servicioLogin.isLoggedInLocalStorage == true){
          localStorage.clear();
          
        } else {
          console.log("no hay");
        }
      }
  );
  }

  getSession(){
    this._servicioLogin.isLoggedInServer().subscribe((data)=>{
      console.log(data);
    });
  }

 
}
